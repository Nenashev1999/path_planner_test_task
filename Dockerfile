FROM ros:noetic-ros-core-focal

RUN apt-get update && apt-get install --no-install-recommends -y \
    build-essential \
    python3-rosdep \
    python3-rosinstall \
    vim-gtk3 \
    git \
    net-tools \
    && rm -rf /var/lib/apt/lists/*

RUN rosdep init && \
  rosdep update --rosdistro $ROS_DISTRO

RUN apt-get update && apt-get install -y --no-install-recommends \
    ros-noetic-desktop-full \
    ros-noetic-tf2 \
    ros-noetic-ompl \
    ros-noetic-tf2-geometry-msgs \
    && rm -rf /var/lib/apt/lists/*

ENV CATKIN_WS=/home/catkin_ws

RUN rm /bin/sh \
    && mkdir -p /home/catkin_ws/src/path_planner_test_task \
    && ln -s /bin/bash /bin/sh 

RUN source /opt/ros/noetic/setup.bash \
    && cd $CATKIN_WS/src \
    && catkin_init_workspace \
    && git clone https://github.com/stonier/cost_map.git \
    && git clone https://gitlab.com/Nenashev1999/path_planner_test_task.git \
    && cd cost_map && rm -rf cost_map_demos && cd .. \
    && apt update \
    && rosdep install -iy --as-root pip:false --reinstall --from-path cost_map

RUN cd $CATKIN_WS \
    && source /opt/ros/noetic/setup.bash \
    && catkin_make 

