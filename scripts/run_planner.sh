#/usr/bin/env bash

export ROS_PACKAGE_PATH=/home/catkin_ws/src
export ROS_MASTER_URI=http://localhost:11311
export ROS_HOSTNAME=localhost
source "/opt/ros/noetic/setup.bash"
source "/home/catkin_ws/devel/setup.bash"
# roscore
roslaunch path_planner_test_task full.launch
