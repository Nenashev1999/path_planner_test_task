# path_planner_test_task

This project is a realization path planner using C++, ROS, and OPML libraries.

## Table Of Contents
1. [Nodes overview](#Nodes overview)  
2. [Installation](#Installation) 
3. [Docker building](#Docker building) 
4. [Docker ready image](#Docker ready image) 
5. [Building using ROS](#Building using ROS) 
6. [User Guide](#User Guide) 
7. [User Guide ROS](#User Guide ROS)
 
## Nodes overview
- path_planner: this is a node where used functions of OMPL library
- map_server: it is a map server based on cost_map_server

## Installation

After cloning repo you can use two options:
- Docker
- Preinstalled ROS + installing dependencies 

## Docker building

For building docker go to path_planner_test_task folder and type:
```
docker-compose up
```
Docker automatically build the image with the installation of all dependencies

## Docker ready image

Another option is using prebuild image. You can download this image [Docker repo](https://hub.docker.com/repository/docker/nenashev1999/path_planner_task)
```
docker pull nenashev1999/path_planner_task
```
After downloading  just go to path_planner_test_task folder and run the docker:
```
docker-compose up
```

## Building using ROS
Before building you must install all dependencies, first in your workspace/src type for downloading :
```
git clone https://github.com/stonier/cost_map.git
git clone https://gitlab.com/Nenashev1999/path_planner_test_task.git
cd cost_map && rm -rf cost_map_demos && cd ..
```
Then install the dependencies:
```
rosdep install -iy --as-root pip:false --reinstall --from-path path_planner_test_task
```
Final step is building
```
cd ~/catkin_ws && catkin_make
```

## User Guide Docker 
If you are using docker, after running "docker-compose up" you run container with GUI, but sometimes RVIZ doesn't work in every computer, if RVIZ gives error, run it manually from system:
```
roslaunch path_planner_test_task run_apps.launch
```
After launching you can use dynamic-reconfigure for choosing the algorithm of searching, state space. Also, you can set the min radius of turning and the time of searching.
For setting the start and goal point use RVIZ pose_estimation and nav_goal instruments

## User Guide ROS 
After running the main launch file run_planner.launch you can use RVIZ for visualizing and setting initial and goal points, for this run:
```
roslaunch path_planner_test_task run_planner.launch
roslaunch path_planner_test_task run_apps.launch
```
After launching you can use dynamic-reconfigure for choosing the algorithm of searching, state space. Also, you can set the min radius of turning and the time of searching.
For setting the start and goal point use RVIZ pose_estimation and nav_goal instruments



