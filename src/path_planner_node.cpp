#include <ros/ros.h>
#include <path_planner.h>
#include <dynamic_reconfigure/server.h>
#include <path_planner_test_task/DynRecConfig.h>


int main(int argc, char **argv)
{
    ros::init(argc, argv, "path_planner_node");
    ros::NodeHandle node;
    ros::Rate loop_rate(30);
    std::string map_topic;
    std::string frame_id;

    ROS_ASSERT(node.hasParam("map_server/topic"));
    ROS_ASSERT(node.hasParam("map_server/frame_id"));
    node.getParam("map_server/topic", map_topic);
    node.getParam("map_server/frame_id", frame_id);

    PathPlanner path_planner(frame_id);
    /****************************************
    ** Pub
    ****************************************/
    path_planner.main_path_pub = node.advertise<nav_msgs::Path>("planner/main_path", 2);

    /****************************************
    ** Sub
    ****************************************/
    ros::Subscriber goal_sub = node.subscribe("/move_base_simple/goal", 1, &PathPlanner::get_goal_callback, &path_planner);
    ros::Subscriber start_point_sub = node.subscribe("/initialpose", 1, &PathPlanner::get_start_position_callback, &path_planner);
    ros::Subscriber occ_map_sub = node.subscribe(map_topic, 1, &PathPlanner::get_grid_callback, &path_planner);
    
    dynamic_reconfigure::Server<path_planner_test_task::DynRecConfig> server;
    server.setCallback(boost::bind(&PathPlanner::params_callback, &path_planner, _1, _2));

    while (node.ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
    }
    return EXIT_SUCCESS;
}

