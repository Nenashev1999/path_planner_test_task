
#include <ros/ros.h>
#include <cstdlib>
#include <cost_map_ros/cost_map_ros.hpp>
#include <nav_msgs/OccupancyGrid.h>
#include <map_server.h>


int main(int argc, char **argv) 
{
    ros::init(argc, argv, "map_server_node");
    ros::NodeHandle node;
    /****************************************
    ** Params
    ****************************************/
    double inscribed_radius;
    double inflation_exponential_rate;
    double inflation_radius;
    std::string inflation_layer;
    std::string static_layer;
    int rate;
    ROS_ASSERT(node.hasParam("map_server/rate"));
    ROS_ASSERT(node.hasParam("map_server/inscribed_radius"));
    ROS_ASSERT(node.hasParam("map_server/inflation_exponential_rate"));
    ROS_ASSERT(node.hasParam("map_server/inflation_radius"));
    ROS_ASSERT(node.hasParam("map_server/inflation_layer"));
    ROS_ASSERT(node.hasParam("map_server/static_layer"));
    node.getParam("map_server/inscribed_radius", inscribed_radius);
    node.getParam("map_server/inflation_exponential_rate", inflation_exponential_rate);
    node.getParam("map_server/inflation_radius", inflation_radius);
    node.getParam("map_server/rate", rate);
    node.getParam("map_server/inflation_layer", inflation_layer);
    node.getParam("map_server/static_layer", static_layer);
    ros::Rate loop_rate(rate);
    /****************************************
    ** Env
    ****************************************/
    std::string image_resource_name = argv[1];
    MapServer map_server(image_resource_name, inscribed_radius, inflation_exponential_rate, inflation_radius, static_layer, inflation_layer);

    /****************************************
    ** Pub
    ****************************************/
    map_server.static_publisher = node.advertise<nav_msgs::OccupancyGrid>("cost_map_server/static_layer", 1, true);
    map_server.inflation_publisher = node.advertise<nav_msgs::OccupancyGrid>("cost_map_server/inflation_layer", 1, true);
    map_server.cost_map_publisher = node.advertise<cost_map_msgs::CostMap>("cost_map_server/cost_map", 1, true);
    /****************************************
    ** Sub
    ****************************************/

    map_server.UpdateMap();
    while (node.ok()) 
    {
        map_server.UpdateMap();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return EXIT_SUCCESS;
}
