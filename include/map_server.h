#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <cost_map_ros/cost_map_ros.hpp>
#include <tf/transform_datatypes.h>
#include <string>

class MapServer 
{
public:
    ros::Publisher static_publisher;
    ros::Publisher inflation_publisher;
    ros::Publisher cost_map_publisher;

    MapServer(
        std::string static_costs_image_resource_name_, 
        double inscribed_radius_, 
        double inflation_exponential_rate_,
        double inflation_radius_,
        std::string static_layer_,
        std::string inflation_layer_)
    {
        static_costs_image_resource_name = static_costs_image_resource_name_;
        inscribed_radius = inscribed_radius_;
        inflation_exponential_rate = inflation_exponential_rate_;
        inflation_radius = inflation_radius_;
        static_layer = static_layer_;
        inflation_layer = inflation_layer_;
        std::shared_ptr<cost_map::LoadImageBundle> loader;
        loader = std::make_shared<cost_map::LoadImageBundle>(static_costs_image_resource_name);
        costmap = *(loader->cost_map);
        inflation_computer = new cost_map::ROSInflationComputer(inscribed_radius, inflation_exponential_rate);
        inflate("static_layer", inflation_layer,
                inflation_radius,
                *inflation_computer,
                costmap);
    }

    void Inflate() 
    {
        inflate("static_layer", "static_inflation_layer",
                inflation_radius,
                *inflation_computer,
                costmap);
    }

    void PublishStaticLayer() 
    {
        nav_msgs::OccupancyGrid msg;
        cost_map::toOccupancyGrid(costmap, "static_layer", msg);
        static_publisher.publish(msg);
    }

    void PublishInflationLayer() 
    {
        nav_msgs::OccupancyGrid msg;
        cost_map::toOccupancyGrid(costmap, "static_inflation_layer", msg);
        inflation_publisher.publish(msg);
    }

    void publish_cost_map()
    {
        cost_map_msgs::CostMap msg;
        cost_map::toMessage(costmap, msg);
        cost_map_publisher.publish(msg);
    }

    void PublishChanges() 
    {
        PublishStaticLayer();
        PublishInflationLayer();
        publish_cost_map();
    }

    void UpdateMap() 
    {
        Inflate();
        PublishChanges();
    }

private:
    std::string static_costs_image_resource_name; 
    std::string inflation_layer;
    std::string static_layer;
    double inscribed_radius;
    double inflation_exponential_rate;
    double inflation_radius;
    cost_map::CostMap costmap;
    cost_map::Inflate inflate;
    cost_map::ROSInflationComputer* inflation_computer;
    



};