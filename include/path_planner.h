#include <ros/ros.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/OccupancyGrid.h>

#include <tf/tf.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Quaternion.h>

#include <cost_map_ros/converter.hpp>
#include <cost_map_ros/cost_map_ros.hpp>

#include <ompl/base/spaces/RealVectorStateSpace.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/base/spaces/DubinsStateSpace.h>
#include <ompl/base/spaces/ReedsSheppStateSpace.h>
#include <ompl/base/spaces/SE2StateSpace.h>

#include <ompl/geometric/planners/rrt/RRT.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/planners/prm/PRM.h>

#include <path_planner_test_task/DynRecConfig.h>
#include <iostream>


class PathPlanner
{
public:
    struct Node
    {
        double x;
        double y;
        double theta;
    };
    ros::Publisher main_path_pub;

    PathPlanner(std::string frame_id_)
    {
        frame_id = frame_id_;
    }

    void params_callback(path_planner_test_task::DynRecConfig &config, uint32_t level)
    {
        space_select = config.State_space;
        planner_select = config.Algorithm;
        path_simplifier = config.Use_Path_Simplifier;
        time_calc = config.Calc_Time;
        turn_radius = config.Turning_Min_Radius;
    }

    void plan()
    {
        std::shared_ptr<ompl::base::SE2StateSpace> space = nullptr;
        switch(space_select)
        {
        case 0:
                space = std::make_shared<ompl::base::SE2StateSpace>();
            break;
        case 1:
                space = std::make_shared<ompl::base::DubinsStateSpace>(turn_radius);
            break;
        case 2:
                space = std::make_shared<ompl::base::ReedsSheppStateSpace>(turn_radius);
            break;
        default:
                space = std::make_shared<ompl::base::SE2StateSpace>();
        };
        ompl::base::RealVectorBounds bounds(2);
        bounds.setLow(0, map->info.origin.position.x);
        bounds.setHigh(0, map->info.origin.position.x + map->info.width * map->info.resolution);
        bounds.setLow(1, map->info.origin.position.y);
        bounds.setHigh(1, map->info.origin.position.y + map->info.height * map->info.resolution);
        space->setBounds(bounds);

        ompl::geometric::SimpleSetup ss(space);
        ss.setStateValidityChecker([this](const ompl::base::State *state) { return this->is_valid_point(state);});
        ompl::base::ScopedState<> start_point_planner(space), goal_point_planner(space);

        conv_state(start_point_planner(), start_point);
        conv_state(goal_point_planner(), goal_point);

        switch(planner_select)
        {
            case 0:
                ss.setPlanner(std::make_shared<ompl::geometric::RRT>(ss.getSpaceInformation()));
                break;
            case 1:
                ss.setPlanner(std::make_shared<ompl::geometric::RRTConnect>(ss.getSpaceInformation()));
                break;
            case 2:
                ss.setPlanner(std::make_shared<ompl::geometric::PRM>(ss.getSpaceInformation()));
                break;
        };
        ss.setStartAndGoalStates(start_point_planner, goal_point_planner);
        ss.setup();
        try
        {
            ompl::base::PlannerStatus solved = ss.solve(time_calc);
            if (solved)
            {
                std::cout << "Found solution:" << std::endl;
                if (path_simplifier)
                {
                    ss.simplifySolution();
                }
                ompl::geometric::PathGeometric path = ss.getSolutionPath();
                path.interpolate(100);
                create_ROS_path(full_path, path, angle, frame_id);
                main_path_pub.publish(full_path);
            }
            else
            {
                ROS_WARN("PATH WAS NOT FOUND");
            }
        }
        catch(const std::exception& e)
        {
            ROS_WARN("PATH WAS NOT FOUND");
        }
        
        
    }

    void create_ROS_path(nav_msgs::Path &path_msg, ompl::geometric::PathGeometric &path, geometry_msgs::Quaternion &angle, std::string frame) 
    {
        path_msg.header.stamp = ros::Time::now();
        path_msg.header.frame_id = frame;
        path_msg.poses.clear();
        geometry_msgs::PoseStamped p;
        int length = path.getStateCount();
        for (int i = 0; i < length; ++i) 
        {
            auto new_state = path.getState(i);
            const auto *s = new_state->as<ompl::base::SE2StateSpace::StateType>();
            p.header.stamp = path_msg.header.stamp;
            p.header.frame_id = path_msg.header.frame_id;
            p.pose.position.x = s->getX();
            p.pose.position.y = s->getY();
            p.pose.position.z = 0;
            tf2::Quaternion quat;
            quat.setRPY(0, 0, s->getYaw());
            quat=quat.normalize();
            p.pose.orientation = tf2::toMsg(quat);
            path_msg.poses.push_back(p);
        }
    }

    void conv_state(ompl::base::State* state, Node pose)
    {
        ompl::base::SE2StateSpace::StateType *s =
        state->as<ompl::base::SE2StateSpace::StateType>();
        s->setX(pose.x);
        s->setY(pose.y);
        s->setYaw(pose.theta);
    }

    void get_start_position_callback(const geometry_msgs::PoseWithCovarianceStamped &msg)
    {  
        tf::Quaternion quat(
        msg.pose.pose.orientation.x,
        msg.pose.pose.orientation.y,
        msg.pose.pose.orientation.z,
        msg.pose.pose.orientation.w);
        tf::Matrix3x3 matrix(quat);
        double roll, pitch, yaw;
        matrix.getRPY(roll, pitch, yaw);
        start_point.x = msg.pose.pose.position.x;
        start_point.y = msg.pose.pose.position.y;
        start_point.theta = yaw;
        ROS_INFO("GOT THE START POINT: [%f %f %f]", start_point.x, start_point.y, start_point.theta);
    }

    void get_goal_callback(const geometry_msgs::PoseStamped &msg)
    {
        tf::Quaternion quat(
        msg.pose.orientation.x,
        msg.pose.orientation.y,
        msg.pose.orientation.z,
        msg.pose.orientation.w);
        tf::Matrix3x3 matrix(quat);
        double roll, pitch, yaw;
        matrix.getRPY(roll, pitch, yaw);
        goal_point.x = msg.pose.position.x;
        goal_point.y = msg.pose.position.y;
        goal_point.theta = yaw;
        if (is_valid_point(goal_point) && is_valid_point(start_point))
        {
            ROS_INFO("GOT THE GOAL POINT: [%f %f %f]", goal_point.x, goal_point.y, goal_point.theta);
                plan();    
        }
        else
        {
            ROS_WARN("INVALID START OR GOAL POINT [%f %f %f], [%f %f %f]", start_point.x, start_point.y, start_point.theta, goal_point.x, goal_point.y, goal_point.theta);
        }
    }

    void get_grid_callback(const nav_msgs::OccupancyGrid::ConstPtr &msg)
    {
        map = msg;
    }

private:
    nav_msgs::OccupancyGrid::ConstPtr map;
    int space_select;
    int planner_select;
    bool path_simplifier;
    double time_calc;
    double turn_radius;
    std::string frame_id;
    int occupied_const = 253;
    Node start_point, goal_point;
    nav_msgs::Path full_path;
    geometry_msgs::Quaternion angle;

    bool is_valid_point(const ompl::base::State *current_state)
    {
        const auto *s = current_state->as<ompl::base::SE2StateSpace::StateType>();
        int x = int((s->getX() - map->info.origin.position.x) / map->info.resolution);
        int y = int((s->getY() - map->info.origin.position.y) / map->info.resolution);
        if (int(map->data[x + map->info.width * y]) > 0)
        {
            return false;
        }
        if (x < 0 || x > map->info.width || y < 0 || y > map->info.height)
        {
            // ROS_WARN("POINT OUTSIDE THE MAP %f %f", s->getX(), s->getY());
            return false;
        }
        return true;
    }

    bool is_valid_point(Node pose)
    {
        int x = int((pose.x - map->info.origin.position.x) / map->info.resolution);
        int y = int((pose.y - map->info.origin.position.y) / map->info.resolution);
        if (x < 0 || x > map->info.width || y < 0 || y > map->info.height)
        {
            ROS_WARN("POINT OUTSIDE THE MAP %f %f", pose.x, pose.y);
            return false;
        }
        if (int(map->data[x + map->info.width * y]) > 0)
        {
            return false;
        }
        return true;
    }
}; 